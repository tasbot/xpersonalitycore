# XPersonalityCore

*The heart and soul of TASBot-X.*

XPersonalityCore (XPC) is a standard and collection of tools that together fill TASBot's physical body with emotion. (Or, well, whatever he feels, anyway.) XPC's scope does *not* include TAS replay, which is governed by individual replay devices (of which the most prominent at time of writing is [TAStm32](https://github.com/ownasaurus/tastm32)).

This repo contains the XPC standard and includes the primary collection of tools fulfilling that standard as submodules. This will likely comprise:
- Physical eye-LED controller hardware and firmware
- eye-LED HAL with extensible bindings (C as a baseline) to control either the physical hardware or a software simulator.
- Reference eye-LED payload (I don't like the naming of that, let's change that later) that uses the HAL to animate TASBot's eyes.

***Repo is under construction and subject to change.***